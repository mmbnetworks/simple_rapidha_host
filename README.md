# README #

simple_rapidha_host is a basic C application designed to run on a linux host and communicate via serial with a virtual host.

### How do I get set up? ###

#### Compile ####

gcc -o simple_host main.c

#### Raspberry Pi Plate Setup ####

1. Download and setup [raspbian](http://www.raspberrypi.org/downloads/)
2. Disable the serial port console [using these steps](http://www.hobbytronics.co.uk/raspberry-pi-serial-port).
3. Install this program.

### How to run it ###

./simple_host -s /dev/ttyUSB0


### Program output example ###

fw: RapidHA v1.5.7

### Who do I talk to? ###

* Repo Owner: David Smith
