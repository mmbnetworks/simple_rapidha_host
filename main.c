#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>  //Sleep
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <inttypes.h>
#include <stdbool.h>

//#define UART_PORT "/dev/tty.SLAB_USBtoUART"
//#define UART_PORT "/dev/tty.usbmodem14241331"
//#define UART_PORT "/dev/ttyAMA0"
char *uart_port = NULL;
#define SOF 0xF1

typedef struct {
   uint8_t idx;
   uint8_t flags;
   uint8_t ph;
   uint8_t sh;
   uint8_t seq;
   uint8_t len;
   uint8_t *payload;
   uint16_t cs;
   uint16_t calc_cs;
} rha_frame_t;

#define SOF_RX (1 << 0)
#define PH_RX (1 << 1)
#define SH_RX (1 << 2)
#define SEQ_RX (1 << 3)
#define LEN_RX (1 << 4)
#define PAYLOAD_RX (1 << 5)

rha_frame_t *frame = NULL;

typedef struct {
  uint8_t ph;
  uint8_t sh;
  uint8_t min_length;
  void (*fn)(rha_frame_t *);
} rha_frame_handler_t;

uint16_t calc_fcs(uint8_t *p_msg, uint8_t len) {
   uint16_t result = 0;
   while(len--)
      result += *p_msg++;
   return result;
}

void version_handler(rha_frame_t *frame) {
  uint8_t major = frame->payload[0];
  uint8_t minor = frame->payload[1];
  uint8_t build = frame->payload[2];
  uint16_t app_info = frame->payload[3] + (frame->payload[4] << 8);
  
  printf("fw: ");
  if (app_info == 0x0002) {
    printf("RapidHA ");
  }
  printf("v%u.%u.%u\n", major, minor, build);
}

void default_handler(rha_frame_t *frame) {
  printf("rx - ph: 0x%" PRIx8 ", sh: 0x%" PRIx8 ", seq: 0x%" PRIx8 ", len: 0x%" PRIx8 , frame->ph, frame->sh, frame->seq, frame->len);
  if (0 < frame->len) {
    printf(", data: ");
    size_t idx = 0;
    for (idx = 0; idx < frame->len; idx++) {
      if (idx > 0) { printf(", "); }
      printf("0x%" PRIx8, frame->payload[idx]);
    }
  }
  printf("\n");
}

rha_frame_handler_t handlers[] = {
  {0x55, 0x03, 15, version_handler}
};


void process_rha_frame(rha_frame_t *frame) {
  size_t handler_count = sizeof(handlers) / sizeof(rha_frame_handler_t);
  size_t idx;
  bool handled = false;
  for (idx = 0; idx < handler_count; idx++) {
    rha_frame_handler_t *handler = &handlers[idx];
    if (handler->ph == frame->ph && handler->sh == frame->sh)
      if (handler->min_length <= frame->len) {
        handler->fn(frame);
        handled = true;
      }
  }

  if (!handled)
    default_handler(frame);
}

void rha_bytes(uint8_t *bytes, uint8_t len) {

   while (len--) {
     uint8_t b = *bytes++;
     if (NULL == frame)
       frame = calloc(sizeof(rha_frame_t), 1);

     if (NULL == frame)
       continue;	//Essentially We're Dropping The Byte

     if (0 == (frame->flags & SOF_RX)) {
       if (SOF == b)
         frame->flags |= SOF_RX;
       continue;
     } 

     frame->calc_cs += b;
     if (0 == (frame->flags & PH_RX)) {
       frame->ph = b;
       frame->flags |= PH_RX;
     } else if (0 == (frame->flags & SH_RX)) {
       frame->sh = b;
       frame->flags |= SH_RX;
     } else if (0 == (frame->flags & SEQ_RX)) {
       frame->seq = b;
       frame->flags |= SEQ_RX;
     } else if (0 == (frame->flags & LEN_RX)) {
       frame->len = b;
       frame->flags |= LEN_RX;
       frame->idx = 0;
       if (0 < frame->len) {
         //Need To Allocate Space For Payload
         frame->payload = calloc(frame->len, 1);
         if (NULL == frame->payload) {
           free(frame);
           frame = NULL;
         }
       } else {
         frame->flags |= PAYLOAD_RX;
       }
     } else if (0 == (frame->flags & PAYLOAD_RX)) {
       frame->payload[frame->idx++] = b;
       if (frame->len <= frame->idx) {
         frame->flags |= PAYLOAD_RX;
         frame->idx = 0;
       }
     } else {
       //In Checksum
       frame->calc_cs -= b;
       frame->cs += (b << (8 * frame->idx++));
       if (2 <= frame->idx) {
         //have the checksum
         if (frame->cs == frame->calc_cs) {
           process_rha_frame(frame);
         }
        
         //Free The Frame
         if (NULL != frame->payload) {
           free(frame->payload);
         }
         free(frame);
         frame = NULL;
       }
     }
   }

}

int parse_args(int argc, char** argv) {
  
  int opt, ret = -1;
  
  while ((opt = getopt (argc, argv, "hs:")) != -1) {
    size_t len = (NULL != optarg) ? strlen(optarg) : 0;
    switch (opt) {
      case 's':
        printf ("Serial device name: \"%s\"\n", optarg);
        if (NULL != uart_port)
           free(uart_port);
        uart_port = NULL;
        uart_port = (char *)calloc(1, len+1);

        strncpy(uart_port, optarg, len);
        ret = 0;        
        break;
      case 'h':
        printf("Usage: %s [OPTIONS]\n", argv[0]);
        printf("  -s device          device\n");
        printf("  -h, --help         print this help and exit\n");
        printf("\n");
        break;
    }
  }
  return ret;
}

int main(int argc, char** argv) {
   struct termios options;
   uint8_t buf[11];
   uint16_t cs;

   if(parse_args(argc, argv))
	return -1;	

   int zb = open(uart_port, O_RDWR | O_NOCTTY | O_NDELAY);
   if (0 > zb) {
      fprintf(stderr, "Error opening port: %s\n", uart_port);
      return -1;
   }

   // Enable Non-Blocking Reads
   fcntl(zb, F_SETFL, FNDELAY);

   // Set Baud Rate
   tcgetattr(zb, &options);
   cfsetispeed(&options, B115200);
   cfsetospeed(&options, B115200);

   // enable receiver
   options.c_cflag |= (CLOCAL | CREAD);

   // 8N1
   options.c_cflag &= ~PARENB;
   options.c_cflag &= ~CSTOPB;
   options.c_cflag &= ~CSIZE;
   options.c_cflag |= CS8;

   // No Hardware Flow Control
   options.c_cflag &= ~CRTSCTS;

   // No Software Flow Control
   options.c_iflag &= ~(IXON | IXOFF | IXANY);

   // Disable Weird stuff link CR mappings
   options.c_iflag &= ~(INLCR | ICRNL | IGNCR);

   // Raw Input
   options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

   // Raw Output
   options.c_oflag &= ~OPOST;

   tcsetattr(zb, TCSANOW, &options);

   //Actually Write Something
   buf[0] = SOF;
   buf[1] = 0x55; // Utility Header
   buf[2] = 0x02; // Module Info Request
   buf[3] = 0x00; // Frame Sequence Number
   buf[4] = 0x00; // Payload Length
   cs = calc_fcs(&buf[1], 4); 
   buf[5] = (uint8_t)(cs & 0xff);
   buf[6] = (uint8_t)((cs >> 8) & 0xff);
   write(zb, buf, 7);

   usleep(10000);

   //Read Stuff
   while (0 < read(zb, &buf, 1)) {
      rha_bytes(buf, 1);
   }

   close(zb);
   return 0;
}
